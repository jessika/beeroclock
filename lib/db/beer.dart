import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';

// import 'package:sqflite/sqflite.dart';
// import './db.dart';
// import '../models/beer.dart';

// Future<void> insertBeer(Beer beer) async {
//   final Database db = await database;
//   await db.insert(
//     'beers',
//     beer.toMap(),
//     conflictAlgorithm: ConflictAlgorithm.fail
//   );
// }

// Future<List<Beer>> beers() async {
//   final Database db = await database;
//   List<Map<String, dynamic>> maps;

//   maps = await db.query('beers');

//   return List.generate(maps.length, (i) {
//     return Beer(
//       // id: 1,
//       name: maps[i]['name'],
//       type: maps[i]['type'],
//       pct: maps[i]['pct'],
//     );
//   });
// }

// {
// "datasetid": "open-beer-database@public-us",
// "recordid": "21427b5076ea6e6adf0f997b460f0c822b0dcdc9",
// "fields": {
//  "brewery_id": "842", "city": "Mill Creek",
//  "name": "Porter", "cat_name": "Irish Ale",
//  "country": "United States", "cat_id": "2",
//  "upc": 0, "coordinates": [47.8774, -122.211],
//  "srm": 0, "last_mod": "2010-07-22T22:00:00+02:00",
//  "state": "Washington", "add_user": "0", "abv": 0.0,
//  "address1": "13300 Bothell-Everett Highway #304",
//  "name_breweries": "McMenamins Mill Creek",
//  "style_name": "Porter",
//  "id": "716", "ibu": 0, "style_id": "25"
// },
// "geometry": {
//    "type": "Point", "coordinates": [-122.211, 47.8774]
// },
// "record_timestamp": "2016-09-26T06:21:38.074+02:00"}
class Beer {
  final int id;
  final String name;
  final String type;
  final double pct;

  Beer({ this.id, this.name, this.pct, this.type });

  factory Beer.fromJson(Map<String, dynamic> json) {
    return new Beer(
      // id: json['id'],
      name: json['name'],
      type: json['style_name'],
      pct: json['abv'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      // 'id': 1,
      'name': name,
      'type': type,
      'pct': pct,
    };
  }
}

Map<String, dynamic> parseJson (String response) {
  // From own beer.json, [id]: {}
  // final parsed = new Map<String, dynamic>.from(json.decode(response));
  // return parsed.map((key, value) => MapEntry(key.toString(), Beer.fromJson(value)));
  // From open-beer-database.json: [{ [] }]
  final parsed = json.decode(response);
  return Map.fromIterable(parsed, key: (e) => e['fields']['id'], value: (e) => Beer.fromJson(e['fields']));
}

Future<Map<String, dynamic>> beers(context) async {
  // var result = await DefaultAssetBundle.of(context).loadString('assets/beers.json');
  var result = await DefaultAssetBundle.of(context).loadString('assets/open-beer-database.json');

  return parseJson(result.toString());
}

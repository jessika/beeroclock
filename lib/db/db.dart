import 'dart:async';

import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

Future<Database> initDb(bool clean) async {
  String dbPath = await getDatabasesPath();
  print('init db with clean=$clean');
  if (clean) {
    deleteDatabase(join(dbPath, 'beer_database.db'));
  }
  return openDatabase(
    join(dbPath, 'beer_database.db'),
    version: 1,
    onCreate: (db, version) {
      var batch = db.batch();
      // if (clean) {
      //   batch.execute('drop table if exists beers');
      //   batch.execute('drop table if exists history');
      // }
      print('onCreate with clean=$clean');
      batch.execute('''
        CREATE TABLE beers(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name TEXT,
          type TEXT,
          pct DOUBLER
        )''');
      batch.execute('''
        CREATE TABLE ratings(
          beerId INTEGER,
          rating INTEGER
        )''');
      batch.execute('''
        CREATE TABLE history(
          beerId INTEGER,
          time TEXT PRIMARY KEY,
          amount DOUBLE,
          rating INTEGER
        )''');

      return batch.commit();
    }
  );
}

Future<Database> database = initDb(false);


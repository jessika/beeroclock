import 'dart:async';

import 'package:sqflite/sqflite.dart';

import './db.dart';
import '../models/entry.dart';
import '../models/beer-rating.dart';

Future<void> insertEntry(Entry entry) async {
  final Database db = await database;
  await db.insert(
    'history',
    entry.toMap(),
    conflictAlgorithm: ConflictAlgorithm.fail
  );
}

Future<List<Entry>> history() async {
  final Database db = await database;
  List<Map<String, dynamic>> maps;

  maps = await db.query('history');

  return List.generate(maps.length, (i) {
    return Entry(
      time: maps[i]['time'],
      beerId: maps[i]['beerId'],
      amount: maps[i]['amount'],
    );
  });
}

Future<void> insertRating(Rating rating) async {
  final Database db = await database;
  await db.insert(
    'ratings',
    rating.toMap(),
    conflictAlgorithm: ConflictAlgorithm.fail
  );
}

Future<Map<int, int>> ratings() async {
  final Database db = await database;
  List<Map<String, dynamic>> maps;

  maps = await db.query('ratings');

  return Map.fromIterable(maps,
    key: (e) => e['beerId'],
    value: (e) => e['rating'],
  );
}

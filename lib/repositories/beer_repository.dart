import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:validators/validators.dart';

import 'package:beeroclock/models/beer.dart';


Map<String, dynamic> parseJson (String response) {
  final parsed = json.decode(response);
  return Map.fromIterable(parsed, key: (e) => e['fields']['id'], value: (e) => Beer.fromJson(e['fields']));
}

class BeerRepository {
  final BuildContext context;

  BeerRepository({@required this.context}) : assert(context != null);

  Future<Map<String, dynamic>> getBeers() async {
    // final int locationId = await weatherApiClient.getLocationId(city);
    // return weatherApiClient.fetchWeather(locationId);
    var result = await DefaultAssetBundle.of(context).loadString('assets/open-beer-database.json');
    var parsed = parseJson(result.toString());
    // List<String> keys = parsed.keys.toList();
    parsed.removeWhere((k, _) => !isNumeric(k));
    return parsed;
  }
}
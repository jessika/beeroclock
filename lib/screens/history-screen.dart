import 'package:beeroclock/blocs/blocs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import './checkin-screen.dart';
import '../db/history.dart';

class History extends StatefulWidget {
  History({Key key}): super(key: key);

  final String title = 'History';

  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {

  @override
  Widget build(BuildContext context) {
    final beerBloc = BlocProvider.of<BeerBloc>(context);

    return Scaffold(
      appBar: AppBar(title: Text(widget.title)),
      body: Center(
        child: FutureBuilder(
          future: Future.wait([
            history(),
            ratings(),
          ]),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List history = snapshot.data[0];
              Map<int, int> ratings = snapshot.data[1];

              return history.isEmpty
                ? Text('Your throat is dry. Try checking in your first beer')
                : HistoryList(history: history, ratings: ratings);
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            }
            return CircularProgressIndicator();
          }
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                BlocProvider.value(
                  value: beerBloc,
                  child: CheckIn(),
                ),
            ),
          ),
        },
        backgroundColor: Theme.of(context).buttonColor,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}

class HistoryList extends StatelessWidget {
  final List history;
  final Map<int, int> ratings;
  // List _shownList;

  HistoryList({Key key, this.history, this.ratings}) : super(key: key);

  // Widget _buildListItem() {
  //   return ListView.builder(
  //     padding: EdgeInsets.all(16.0),
  //     itemBuilder: (context, i) {
  //       if (index >= _shownList.length) {
  //         _shownList.addAll(history.sublist(i, i + 10))
  //       }
  //     },
  //   )
  // }

  Widget _buildListItem(beers) {
    return ListView.builder(
      itemCount: history == null ? 0 : history.length,
      itemBuilder: (context, index) {
        return _listItem(context, history[index], beers);
      }
    );
  }

  Widget _listItem(context, item, beers) {
    return Card(
        child: Container(
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(Icons.beenhere, color: Theme.of(context).primaryColor),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '${beers[item.beerId.toString()].name}',
                        style: TextStyle(fontSize: 20.0, color: Colors.grey[700]),
                      ),
                      Text(
                        '${item.amount} L',
                        style: TextStyle(fontSize: 15.0, color: Colors.grey[700]),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.star, color: Colors.yellow),
                          Text(
                            '${ratings[item.beerId]}',
                            style: TextStyle( fontSize: 15.0, color: Colors.lightBlueAccent),
                          ),
                        ]),
                  ])
                ),
            ]),
          ),
        padding: const EdgeInsets.all(15.0),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BeerBloc, BeerState>(
      builder: (context, state) {
        if (state is BeerSuccess) {
          return _buildListItem(state.beers);
        } else if (state is BeerLoading) {
          return CircularProgressIndicator();
        }
        return Text('Something went wrong');
      }
    );
  }
}
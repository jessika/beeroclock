import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  Profile({ Key key }) : super(key: key);

  final String title = 'Profile';

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Text('Add a profile'),
      ),
    );
  }
}
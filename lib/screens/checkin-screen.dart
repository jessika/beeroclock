import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

import 'package:beeroclock/blocs/blocs.dart';
import '../db/history.dart';
import '../models/entry.dart';
import '../models/beer-rating.dart';

class CheckIn extends StatefulWidget {
  CheckIn({ Key key }) : super(key: key);

  @override
  _CheckInState createState() => _CheckInState();
}

class _CheckInState extends State<CheckIn> {
  // final amountController = TextEditingController(text: '0.25');
  final _amounts = [0.25, 0.5, 0.75];
  final _ratings = [1, 2, 3, 4, 5];
  List<bool>isSelected = [false, true, false];
  List<bool>isRatingSelected = [false, false, true, false, false];

  double _amount = 0.5;
  int _rating = 3;
  String _beer = '';

  void _add() {
    int beerId = int.tryParse(_beer.split('--')[1]);

    Entry _newEntry = Entry(
      time: DateTime.now().toString(),
      beerId: beerId,
      amount: _amount,
    );

    Rating _newRating = Rating(
      beerId: beerId,
      rating: _rating,
    );

    insertEntry(_newEntry);
    insertRating(_newRating);
  }

  @override
  void initState() {
    super.initState();

    // amountController.addListener(() {
    //   print('new value ${amountController.text}');
    // });
  }

  @override
  void dispose() {
    // amountController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Check in beer'),
      ),
      body: Column(
        children: <Widget>[
          // TextFormField(
          //   controller: amountController,
          // ),
          Row(
            children: [
              BlocBuilder<BeerBloc, BeerState>(
                builder: (context, state) {
                  if (state is BeerSuccess) {
                    List<DropdownMenuItem> items = state.beers.keys.map<DropdownMenuItem>((String id) {
                      return DropdownMenuItem(
                        value: '${state.beers[id].name}--$id',
                        child: Text(
                          '${state.beers[id].name}',
                        ),
                      );
                    }).toList();

                    return SearchableDropdown.single(
                      items: items,
                      value: _beer,
                      onChanged: (value) {
                        print(value);
                        setState(() => {
                          _beer = value,
                        });
                      },
                    );
                  } else if (state is BeerFailure) {
                    return Text('something went wrong');
                  }
                  return CircularProgressIndicator();
                }
              ),
            ]),
          Row(children: [
            Text('Amount', style: TextStyle(fontSize: 20.0),),
            ToggleButtons(
              isSelected: isSelected,
              onPressed: (int index) {
                setState(() {
                  for (int buttonIndex = 0; buttonIndex < isSelected.length; buttonIndex++) {
                    if (buttonIndex == index) {
                      isSelected[buttonIndex] = true;
                      _amount = _amounts[index];
                    } else {
                      isSelected[buttonIndex] = false;
                    }
                  }
                });
              },
              children: [
                Text('${_amounts[0]}'),
                Text('${_amounts[1]}'),
                Text('${_amounts[2]}'),
            ]),
        ]),
        Row(children: [
            Text('Rating', style: TextStyle(fontSize: 20.0),),
            ToggleButtons(
              isSelected: isRatingSelected,
              onPressed: (int index) {
                setState(() {
                  for (int buttonIndex = 0; buttonIndex < isRatingSelected.length; buttonIndex++) {
                    if (buttonIndex == index) {
                      isRatingSelected[buttonIndex] = true;
                      _rating = _ratings[index];
                    } else {
                      isRatingSelected[buttonIndex] = false;
                    }
                  }
                });
              },
              children: [
                Text('${_ratings[0]}'),
                Text('${_ratings[1]}'),
                Text('${_ratings[2]}'),
                Text('${_ratings[3]}'),
                Text('${_ratings[4]}'),
            ]),
        ]),
      ]),
      persistentFooterButtons: <Widget>[
        IconButton(
          icon: Icon(Icons.add),
          onPressed: _add
        )
      ],
    );
  }
}
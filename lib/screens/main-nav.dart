import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:beeroclock/blocs/blocs.dart';
import './profile-screen.dart';
import './history-screen.dart';

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _index = 0;

  final List<Widget> _screens = [
    Profile(),
    History(),
  ];

  void _onTapped(int newIndex) {
    setState(() {
      _index = newIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    final beerBloc = BlocProvider.of<BeerBloc>(context);
    beerBloc.add(BeerRequested());
    
    return Scaffold(
      body: BlocProvider.value(
        value: beerBloc,
        child: _screens[_index]
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _index,
        onTap: _onTapped,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            title: Text('Profile'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.time_to_leave),
            title: Text('History'),
          ),
        ]
      ),
    );
  }
}
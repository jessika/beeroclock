import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import './screens/main-nav.dart';
import 'package:beeroclock/repositories/repositories.dart';
import 'package:beeroclock/blocs/blocs.dart';

void main() {
  runApp(
    MyApp()
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Beer o\'Clock',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
        appBarTheme: AppBarTheme(color: Colors.cyan),
        buttonColor: Colors.pinkAccent,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider<BeerBloc>(
        create: (_) => BeerBloc(beerRepository: BeerRepository(context: context)),
        child: Home(),
      ),
    );
  }
}

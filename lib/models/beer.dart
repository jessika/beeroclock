
class Beer {
  final int id;
  final String name;
  final String type;
  final double pct;

  Beer({ this.id, this.name, this.pct, this.type });

  factory Beer.fromJson(Map<String, dynamic> json) {
    return new Beer(
      // id: json['id'],
      name: json['name'],
      type: json['type'],
      pct: json['pct'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      // 'id': 1,
      'name': name,
      'type': type,
      'pct': pct,
    };
  }
}
// history entry: beerId, time, amount, rating(?), comment, picture

class Entry {
  final int beerId;
  final String time;
  final double amount;
  final int rating;

  Entry({ this.beerId, this.time, this.amount, this.rating});

  Map<String, dynamic> toMap() {
    return {
      'beerId': beerId,
      'time': time,
      'amount': amount,
    };
  }
}
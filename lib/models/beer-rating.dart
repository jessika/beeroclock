
class Rating {
  final int beerId;
  final int rating;

  Rating({ this.beerId, this.rating });

  factory Rating.fromJson(Map<String, dynamic> json) {
    return new Rating(
      beerId: json['beerId'],
      rating: json['rating'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'beerId': beerId,
      'rating': rating,
    };
  }
}
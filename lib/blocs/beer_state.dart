import 'dart:ffi';

import 'package:flutter/cupertino.dart';

abstract class BeerState {
  const BeerState();

  @override
  List get props => [];
}

class BeerInitial extends BeerState {}

class BeerLoading extends BeerState {}

class BeerSuccess extends BeerState {
  final Map<String, dynamic> beers;

  const BeerSuccess({@required this.beers});

  @override
  List get props => [beers];
}

class BeerFailure extends BeerState {}
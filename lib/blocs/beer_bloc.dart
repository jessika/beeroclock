import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'package:beeroclock/repositories/repositories.dart';
import 'package:beeroclock/blocs/blocs.dart';

class BeerBloc extends Bloc<BeerEvent, BeerState> {
  final BeerRepository beerRepository;

  // BeerBloc({@required this.beerRepository}) : assert(beerRepository != null);
  BeerBloc({@required this.beerRepository}) : super(BeerInitial());

  @override
  BeerState get initialState => BeerInitial();

  @override
  Stream<BeerState> mapEventToState(BeerEvent event) async* {
    if (event is BeerRequested) {
      yield BeerLoading();
      try {
        final Map<String, dynamic> beers = await beerRepository.getBeers();
        yield BeerSuccess(beers: beers);
      } catch(_) {
        yield BeerFailure();
      }
    }
  }
}
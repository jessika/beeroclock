
abstract class BeerEvent {
  const BeerEvent();
}

class BeerRequested extends BeerEvent {
  const BeerRequested();

  @override
  List get props => [];
}